package com.rehino.reactor.repos.implementations.redis.config;

import com.rehino.reactor.model.User;
import com.rehino.reactor.repos.implementations.redis.repos.UserRepositoryRedis;
import com.rehino.reactor.repos.interfaces.UserRepository;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.ReactiveRedisOperations;

@Configuration
@ConditionalOnProperty(prefix = "datasource", name = "implementation", havingValue = "redis", matchIfMissing = true)
public class RedisRepositoryConfig {
    @Bean
    UserRepository userRepositoryRedis(ReactiveRedisOperations<String, User> ops) {
        return new UserRepositoryRedis(ops);
    }
}
