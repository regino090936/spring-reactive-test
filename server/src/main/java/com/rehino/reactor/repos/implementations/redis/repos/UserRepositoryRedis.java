package com.rehino.reactor.repos.implementations.redis.repos;

import com.rehino.reactor.model.User;
import com.rehino.reactor.model.UserSerDes;
import com.rehino.reactor.repos.interfaces.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import java.util.UUID;
import java.util.stream.Stream;

public class UserRepositoryRedis implements UserRepository {
    private final Logger logger = LoggerFactory.getLogger(UserRepositoryRedis.class);
    private final ReactiveRedisOperations<String, User> userOps;

    public UserRepositoryRedis(ReactiveRedisOperations<String, User> userOps) {
        this.userOps = userOps;
    }

    @Value("${datasource.initialization-size}")
    private int initialSize;

    @Override
    public void initialize() {
        userOps
                .keys("*")
                .flatMap(userOps.opsForValue()::delete)
                .subscribe();

        Flux
                .fromStream(
                        Stream.iterate(1, i -> i + 1)
                            .limit(initialSize)
                            .map(i -> new User(UUID.randomUUID().toString(), "rdsname" + i, "rdsemail" + i))
                )
                .flatMap(user -> userOps.opsForValue().set(user.getId(), user))
                .thenMany(
                        userOps
                                .keys("*")
                                .flatMap(userOps.opsForValue()::get)
                )
                .subscribe();
    }

    public Flux<User> getUsers() {
        logger.info("requests users");
        return userOps
                .keys("*")
                .flatMap(userOps.opsForValue()::get);
    }

    public Mono<User> getUser(String id) {
        logger.info("requests user with id: {}", id);
        return userOps.opsForValue().get(id);
    }

    public Mono<Boolean> addUser(UserSerDes newUser) {
        final User user = new User(newUser);
        if (user.getName().equals("") || user.getEmail().equals("")) {
            logger.info("bad data. User: {}", user);
            return Mono.just(false);
        }
        logger.info("add new user: {}", user);
        return userOps
                .opsForValue()
                .setIfAbsent(user.getId(), user);
    }

    public Mono<Boolean> editUser(UserSerDes newUser, String id) {
        logger.info("editing user: id - {}, user - {}", id, newUser);
        return userOps.opsForValue().setIfPresent(id, new User(id, newUser));
    }

    public Mono<Boolean> deleteUser(String id) {
        logger.info("deleting user: id - {}", id);
        return userOps.opsForValue().delete(id);
    }
}
