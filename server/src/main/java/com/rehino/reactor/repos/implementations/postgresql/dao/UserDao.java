package com.rehino.reactor.repos.implementations.postgresql.dao;

import com.rehino.reactor.model.User;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
@ConditionalOnProperty(prefix = "datasource", name = "implementation", havingValue = "postgres")
public interface UserDao extends ReactiveCrudRepository<User, String> {

    @Query("insert into users (id, name, email) values(:id, :name, :email)")
    Mono<User> insert(String id, String name, String email);

    @Query("update users set name = :name, email = :email where id = :id")
    Mono<User> update(String id, String name, String email);

}