package com.rehino.reactor.repos.implementations.postgresql.repos;

import com.rehino.reactor.repos.implementations.postgresql.dao.UserDao;
import com.rehino.reactor.repos.interfaces.UserRepository;
import com.rehino.reactor.model.User;
import com.rehino.reactor.model.UserSerDes;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.r2dbc.core.DatabaseClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Stream;

public class UserRepositoryPostgreSql implements UserRepository {

    private final DatabaseClient client;
    private final UserDao userDao;

    @Value("${datasource.initialization-size}")
    private int initialSize;

    public UserRepositoryPostgreSql(DatabaseClient databaseClient, UserDao userDao) {
        this.client = databaseClient;
        this.userDao = userDao;
    }

    @Override
    public void initialize()
    {
        client.sql(
                    "create table if not exists users\n" +
                    "(\n" +
                    "\tid varchar(40) primary key not null,\n" +
                    "\tname varchar(30) not null,\n" +
                    "\temail varchar(30) not null\n" +
                    ");"
                )
                .fetch()
                .rowsUpdated();

        userDao.deleteAll().subscribe();

        Stream.iterate(1, i -> i + 1)
                .limit(initialSize)
                .forEach(i -> addUser(new UserSerDes("name" + i, "email" + i)).subscribe());

    }

    @Override
    public Flux<User> getUsers() {
        return userDao.findAll();
    }

    @Override
    public Mono<User> getUser(String id) {
        return userDao.findById(id);
    }

    @Override
    public Mono<Boolean> addUser(UserSerDes userSerDes) {
        if (userSerDes.getEmail().equals("") || userSerDes.getName().equals(""))
            return Mono.just(false);
        return userDao.insert(
                    UUID.randomUUID().toString(),
                    userSerDes.getName(),
                    userSerDes.getEmail()
                )
                .then(Mono.just(true));
    }

    @Override
    public Mono<Boolean> editUser(UserSerDes userSerDes, String id) {

        return userDao.update(
                    id,
                    userSerDes.getName(),
                    userSerDes.getEmail()
                )
                .then(userDao.findById(id))
                .map(Objects::nonNull);
    }

    @Override
    public Mono<Boolean> deleteUser(String id) {
        return userDao
                .findById(id)
                .flatMap(u -> {
                    if (u.getId().equals("")) {
                        return Mono.just(false);
                    } else {
                        return userDao.deleteById(id).then(Mono.just(true));
                    }
                });
    }
}
