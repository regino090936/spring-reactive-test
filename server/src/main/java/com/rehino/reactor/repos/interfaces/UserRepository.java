package com.rehino.reactor.repos.interfaces;

import com.rehino.reactor.model.User;
import com.rehino.reactor.model.UserSerDes;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface UserRepository {

    void initialize();

    Flux<User> getUsers();

    Mono<User> getUser(String id);

    Mono<Boolean> addUser(UserSerDes userSerDes);

    Mono<Boolean> editUser(UserSerDes userSerDes, String id);

    Mono<Boolean> deleteUser(String id);
}
