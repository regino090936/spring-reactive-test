package com.rehino.reactor.repos.implementations.postgresql.config;

import com.rehino.reactor.repos.implementations.postgresql.dao.UserDao;
import com.rehino.reactor.repos.implementations.postgresql.repos.UserRepositoryPostgreSql;
import com.rehino.reactor.repos.interfaces.UserRepository;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.r2dbc.core.DatabaseClient;

@Configuration
@ConditionalOnProperty(prefix = "datasource", name = "implementation", havingValue = "postgres")
public class PostgreSqlRepositoryConfig {
    @Bean
    UserRepository userRepositoryPostgres(DatabaseClient databaseClient, UserDao userDao) {
        return new UserRepositoryPostgreSql(databaseClient, userDao);
    }
}
