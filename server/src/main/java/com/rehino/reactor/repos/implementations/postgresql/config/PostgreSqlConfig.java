package com.rehino.reactor.repos.implementations.postgresql.config;

import io.r2dbc.postgresql.PostgresqlConnectionConfiguration;
import io.r2dbc.postgresql.PostgresqlConnectionFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.r2dbc.core.DefaultReactiveDataAccessStrategy;
import org.springframework.data.r2dbc.dialect.PostgresDialect;
import org.springframework.data.r2dbc.repository.support.R2dbcRepositoryFactory;
import org.springframework.r2dbc.core.DatabaseClient;
import com.rehino.reactor.repos.implementations.postgresql.dao.UserDao;

@Configuration
public class PostgreSqlConfig {
    @Value("${postgresql.host}")
    private String host;

    @Value("${postgresql.database}")
    private String database;

    @Value("${postgresql.username}")
    private String username;

    @Value("${postgresql.password}")
    private String password;

    @Bean
    PostgresqlConnectionFactory connectionFactory() {
        return new PostgresqlConnectionFactory(
                PostgresqlConnectionConfiguration.builder()
                        .host(host)
                        .database(database)
                        .username(username)
                        .password(password)
                        .build()
        );
    }

    @Bean
    DatabaseClient databaseClient(PostgresqlConnectionFactory factory) {
        return DatabaseClient.create(factory);
    }

    @Bean
    R2dbcRepositoryFactory repositoryFactory(@Qualifier("databaseClient") DatabaseClient client) {
        return new R2dbcRepositoryFactory(client, new DefaultReactiveDataAccessStrategy(new PostgresDialect()));
    }

    @Bean
    UserDao r2dbcUserRepository(R2dbcRepositoryFactory repositoryFactory) {
        return repositoryFactory.getRepository(UserDao.class);
    }
}
