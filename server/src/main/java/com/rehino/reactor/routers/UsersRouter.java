package com.rehino.reactor.routers;

import com.rehino.reactor.hadlers.UsersHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.*;

@Configuration
public class UsersRouter {

    @Bean
    public RouterFunction<ServerResponse> route(UsersHandler usersHandler) {
        return RouterFunctions
                .route(
                        RequestPredicates
                                .GET("api/users")
                                .and(RequestPredicates.contentType(MediaType.ALL)),
                        usersHandler::getUsers
                )
                .andRoute(
                        RequestPredicates
                            .GET("api/users/{id}")
                            .and(RequestPredicates.contentType(MediaType.ALL)),
                        usersHandler::getUser
                )
                .andRoute(
                        RequestPredicates
                            .POST("api/users")
                            .and(RequestPredicates.contentType(MediaType.ALL)),
                        usersHandler::addUser
                )
                .andRoute(
                        RequestPredicates
                            .PUT("api/users/{id}")
                            .and(RequestPredicates.contentType(MediaType.ALL)),
                        usersHandler::editUser
                )
                .andRoute(
                        RequestPredicates
                            .DELETE("api/users/{id}"),
                        usersHandler::deleteUser
                );
    }
}
