package com.rehino.reactor.model;

public class UserSerDes {
    private final String email;
    private final String name;

    public UserSerDes(String name, String email) {
        this.email = email;
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "UserSerDes{" +
                "email='" + email + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
