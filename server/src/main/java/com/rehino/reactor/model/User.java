package com.rehino.reactor.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table("users")
public class User {
    @Id
    private String id;
    private String name;
    private String email;

    public User(UserSerDes userSerDes) {
        this.id = UUID.randomUUID().toString();
        this.name = userSerDes.getName();
        this.email = userSerDes.getEmail();
    }

    public User(String id, UserSerDes userSerDes) {
        this.id = id;
        this.name = userSerDes.getName();
        this.email = userSerDes.getEmail();
    }

    public static User getUser(UserSerDes userSerDes) {
        User user = new User();
        user.setEmail(userSerDes.getEmail());
        user.setName(userSerDes.getName());
        return user;
    }
}
