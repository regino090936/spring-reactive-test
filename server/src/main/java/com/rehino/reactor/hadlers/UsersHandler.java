package com.rehino.reactor.hadlers;

import com.rehino.reactor.repos.interfaces.UserRepository;
import com.rehino.reactor.model.User;
import com.rehino.reactor.model.UserSerDes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

@Component
public class UsersHandler {
    private final UserRepository userRepository;

    @Autowired
    public UsersHandler(UserRepository userRepositoryImplementation) {
        userRepositoryImplementation.initialize();
        this.userRepository = userRepositoryImplementation;
    }

    public Mono<ServerResponse> getUsers(ServerRequest request) {
        return ServerResponse
                .ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(userRepository.getUsers(), User.class);
    }

    public Mono<ServerResponse> getUser(ServerRequest request) {
        return userRepository
                .getUser(request.pathVariable("id"))
                .flatMap(user -> ServerResponse.ok().body(Mono.just(user), User.class))
                .switchIfEmpty(ServerResponse.notFound().build());
    }

    public Mono<ServerResponse> addUser(ServerRequest request) {
        return request
                .bodyToMono(UserSerDes.class)
                .flatMap(userRepository::addUser)
                .flatMap(isSuccess -> isSuccess ?
                        ServerResponse.ok().build() :
                        ServerResponse.badRequest().build()
                );
    }

    public Mono<ServerResponse> editUser(ServerRequest request) {
        return request
                .bodyToMono(UserSerDes.class)
                .flatMap(user -> userRepository.editUser(
                        user,
                        request.pathVariable("id")
                ))
                .flatMap(isSuccess -> isSuccess ?
                        ServerResponse.ok().build() :
                        ServerResponse.notFound().build()
                );
    }

    public Mono<ServerResponse> deleteUser(ServerRequest request) {
        return userRepository.deleteUser(
                        request.pathVariable("id")
                )
                .flatMap(isSuccess -> isSuccess ?
                        ServerResponse.ok().build() :
                        ServerResponse.notFound().build()
                );
    }
}
